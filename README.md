# Game Paper Rock Scissor - WASM.

## Description

the project is compose by two projects, the main is a rust wams. the second is a frontend build with vuejs 3.

## User

1. In the main project execute the command `wasm-pack build`
2. Change to folder web-app and execute the command `npm i`
3. Execute the command `npm run dev`
4. Open the browser and test the game.

## Link

https://rock-paper-scissors-macordoba-9a80d00897ab413bf2f279ab1ffd3ef73.gitlab.io/
