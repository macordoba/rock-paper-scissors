import * as wasm from 'game';

const play_converter = (play: wasm.Play) => {
  switch (play) {
    case wasm.Play.Rock:
      return '✊';
    case wasm.Play.Paper:
      return '🖐️';
    case wasm.Play.Scissor:
      return '✌️';
  }
};

const result_converter = (result: wasm.ResultPlay) => {
  switch (result) {
    case wasm.ResultPlay.Win:
      return '✔️';
    case wasm.ResultPlay.Lost:
      return '❌️';
    case wasm.ResultPlay.Draw:
      return '🟰';
  }
};

const winner_converter = (player: wasm.Player) => {
  switch (player) {
    case wasm.Player.One:
      return 'You';
    case wasm.Player.Two:
      return 'Robot';
  }
};

export { play_converter, result_converter, winner_converter };
