use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Player {
    None,
    One,
    Two,
}

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub enum Play {
    Paper,
    Rock,
    Scissor,
}

#[wasm_bindgen]
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ResultPlay {
    Draw,
    Lost,
    Win,
}

#[wasm_bindgen]
#[derive(PartialEq, Debug)]
pub struct Results {
    pub result_one: ResultPlay,
    pub result_two: ResultPlay,
    pub winner: Player,
}
