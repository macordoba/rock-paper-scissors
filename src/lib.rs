mod structure;

use crate::structure::{Play, Player, ResultPlay, Results};
use rand::Rng;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn calculate(play_player_one: Play, play_player_two: Play) -> Results {
    let values_play = (play_player_one as isize, play_player_two as isize);
    let mut results = (ResultPlay::Draw, ResultPlay::Draw, Player::None);

    if (values_play.0 == 0 && values_play.1 == 1)
        || (values_play.0 == 1 && values_play.1 == 2)
        || (values_play.0 == 2 && values_play.1 == 0)
    {
        results.0 = ResultPlay::Win;
        results.1 = ResultPlay::Lost;
        results.2 = Player::One;
    } else if values_play.0 != values_play.1 {
        results.0 = ResultPlay::Lost;
        results.1 = ResultPlay::Win;
        results.2 = Player::Two;
    }

    Results {
        result_one: results.0,
        result_two: results.1,
        winner: results.2,
    }
}

#[wasm_bindgen]
pub fn generate_moves() -> u8 {
    rand::thread_rng().gen_range(0..=2)
}

#[test]
fn test_cases() {
    assert_eq!(
        calculate(Play::Paper, Play::Paper),
        Results {
            result_one: ResultPlay::Draw,
            result_two: ResultPlay::Draw,
            winner: Player::None
        }
    );

    assert_eq!(
        calculate(Play::Paper, Play::Rock),
        Results {
            result_one: ResultPlay::Win,
            result_two: ResultPlay::Lost,
            winner: Player::One
        }
    );

    assert_eq!(
        calculate(Play::Paper, Play::Scissor),
        Results {
            result_one: ResultPlay::Lost,
            result_two: ResultPlay::Win,
            winner: Player::Two
        }
    );

    assert_eq!(
        calculate(Play::Rock, Play::Paper),
        Results {
            result_one: ResultPlay::Lost,
            result_two: ResultPlay::Win,
            winner: Player::Two
        }
    );

    assert_eq!(
        calculate(Play::Rock, Play::Rock),
        Results {
            result_one: ResultPlay::Draw,
            result_two: ResultPlay::Draw,
            winner: Player::None
        }
    );

    assert_eq!(
        calculate(Play::Rock, Play::Scissor),
        Results {
            result_one: ResultPlay::Win,
            result_two: ResultPlay::Lost,
            winner: Player::One
        }
    );

    assert_eq!(
        calculate(Play::Scissor, Play::Paper),
        Results {
            result_one: ResultPlay::Win,
            result_two: ResultPlay::Lost,
            winner: Player::One
        }
    );

    assert_eq!(
        calculate(Play::Scissor, Play::Rock),
        Results {
            result_one: ResultPlay::Lost,
            result_two: ResultPlay::Win,
            winner: Player::Two
        }
    );

    assert_eq!(
        calculate(Play::Scissor, Play::Scissor),
        Results {
            result_one: ResultPlay::Draw,
            result_two: ResultPlay::Draw,
            winner: Player::None
        }
    );
}
